/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup Delay
 ** @{ */

/* @brief  EDU-CIAA NXP GPIO driver
 * @author Albano Peñalva
 *
 * This driver provide functions to generate delays using Timer0
 * of LPC4337
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 26/11/2018 | Document creation		                         |
 *
 */

#ifndef DISPLAYITS_E0803_H_
#define DISPLAYITS_E0803_H_

/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "gpio.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

//funcion de inicializacion de pines. Recibe un arreglo de GPIO que tiene que tener 7 pines y todos configurados de salida
bool ITSE0803Init(gpio_t * pins);

//función que muestra el valor en el display
bool ITSE0803DisplayValue(uint16_t valor);

//función que devuelve el valor que muestra el display
uint16_t ITSE0803ReadValue(void);

//función Deinit
bool ITSE0803Deinit(gpio_t * pins);


#endif /* DISPLAYITS_E0803_H_ */




