/* Copyright 2021,
 * Gualpa Solange
 * solange.gualpa@gmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup HCSR04
 ** @{ */

/** \brief Driver que realiza medición de distancia a través de un sensor de ultrasonido.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *  SG	        Solange Gualpa
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20211108 v0.0.1 initials initial version
 */
#ifndef _HCSR04_H
#define _HCSR04_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

#include "bool.h"
#include "gpio.h"
#include <stdint.h>
/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/
/** @fn bool HcSr04Init(gpio_t echo, gpio_t trigger)
 * @brief Initialization function of EDU-CIAA distance measurer
 * Mapping ports (PinMux function), set direction and initial state of distance measurer ports
 * @param[in] GPIO Echo and GPIO Trigger
 * @return No parameter
 */
bool HcSr04Init(gpio_t echo, gpio_t trigger);

/** @fn int16_t HcSr04ReadDistanceCentimeters(void)
 * @brief Reader function of EDU-CIAA for distance in centimeters
 * Mapping ports (PinMux function), set direction and initial state of distance in centimeters reader ports
 * @param[in] No parameter
 * @return int16_t parameter
 */
int16_t HcSr04ReadDistanceCentimeters(void);

/** @fn int16_t HcSr04ReadDistanceInches(void)
 * @brief Reader function of EDU-CIAA for distance in inches
 * Mapping ports (PinMux function), set direction and initial state of distance in inches reader ports
 * @param[in] No parameter
 * @return int16_t parameter
 */
int16_t HcSr04ReadDistanceInches(void);

/** @fn bool HcSr04Deinit(gpio_t echo, gpio_t trigger)
 * @brief Deinitialization function of EDU-CIAA distance measurer
 * Mapping ports (PinMux function), set direction and initial state of leds ports
 * @param[in] GPIO Echo and GPIO Trigger
 * @return TRUE if no error
 */
bool HcSr04Deinit(gpio_t echo, gpio_t trigger);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


#endif /* #ifndef _HCSR04_H */

