/* Copyright 2021,
 * Levrino Micaela
 * micaela_levrino@hotmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief Driver que maneja la lectura de datos obtenida a partir de un acelerómetro.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *  ML	        Micaela Levrino
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210609 v0.0.1 initials initial version
 */

/*==================[inclusions]=============================================*/
#include "MMA7260Q.h"

/*==================[macros and definitions]=================================*/
/** @def MAX_VOLTAGE
 * @brief Máximo valor de voltaje en mV que puede medir el driver
 */
#define MAX_VOLTAGE 3300
/** @def MAX_VALUE
 * @brief Máximo valor de bits
 */
#define MAX_VALUE 1024
/** @def OFFSET
 * @brief Mitad del valor máximo de voltaje correspondiente en bits
 */
#define OFFSET 512
/** @def SENSITIVITY
 * @brief Valor de la sensibilidad dado por el driver
 */
#define SENSITIVITY 800

/*==================[internal data declaration]==============================*/

bool conversion; /**< booleando si realiza o no la conversión*/
uint16_t valor; /**< Valor convertido*/
uint8_t canal; /**< Canal de la placa*/

/*==================[internal functions declaration]=========================*/

/**@fn void ReadValue()
 * @brief  Función que lee el valor analógico de determinado canal
 * @return None
 */
void ReadValue();
/**@fn float UnitConvert(uint16_t value)
 * @brief  Función que convierte el valor leído en unidades de gravedad
 * @return valor convertido
 */
float UnitConvert(uint16_t value);

/*==================[internal data definition]===============================*/

gpio_t g_select1_local; /**< pin de selección local*/
gpio_t g_select2_local; /**< pin de selección local*/

/*==================[external data definition]===============================*/

analog_input_config my_ad_x = {CH1,AINPUTS_SINGLE_READ,ReadValue}; /**< Typedef configuración puerto analógico/digital canal x*/
analog_input_config my_ad_y = {CH2,AINPUTS_SINGLE_READ,ReadValue}; /**< Typedef configuración puerto analógico/digital canal y*/
analog_input_config my_ad_z = {CH3,AINPUTS_SINGLE_READ,ReadValue}; /**< Typedef configuración puerto analógico/digital canal z*/

/*==================[internal functions definition]==========================*/

void ReadValue(){
	conversion=false;
	AnalogInputRead(canal,&valor);
}

float UnitConvert(uint16_t value){

	float new_value;

	new_value = value - OFFSET;
	new_value = (float)new_value*MAX_VOLTAGE/MAX_VALUE;
	new_value = (float)new_value/SENSITIVITY;

	return new_value;
}

/*==================[external functions definition]==========================*/

bool MMA7260QInit(gpio_t gSelect1, gpio_t gSelect2){

	GPIOInit(gSelect1, GPIO_OUTPUT);
	GPIOInit(gSelect2, GPIO_OUTPUT);

	g_select1_local=gSelect1;
	g_select2_local=gSelect2;

	GPIOOff(g_select1_local);
	GPIOOff(g_select2_local);

	return 1;
}

float ReadXValue(){

	AnalogInputInit(&my_ad_x);
//	AnalogInputReadPolling(CH1,&value);
	conversion = true;
	canal = CH1;
	AnalogStartConvertion();
	while(conversion){
	}

	return UnitConvert(valor);
}

float ReadYValue(){

	AnalogInputInit(&my_ad_y);
//	AnalogInputReadPolling(CH2,&value);
	conversion = true;
	canal = CH2;
	AnalogStartConvertion();
	while(conversion){
	}

	return UnitConvert(valor);
}

float ReadZValue(){

	AnalogInputInit(&my_ad_z);
//	AnalogInputReadPolling(CH3,&value);
	conversion = true;
	canal = CH3;
	AnalogStartConvertion();
	while(conversion){
	}

	return UnitConvert(valor);
}

bool MMA7260QDeInit(gpio_t gSelect1, gpio_t gSelect2){

	GPIODeinit();
	return 1;
}
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
