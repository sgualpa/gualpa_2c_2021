/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/HcSr04.h"       /* <= own header */
#include "delay.h"


/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/
gpio_t pin_trigger, pin_echo;

bool HcSr04Init(gpio_t echo, gpio_t trigger){
	GPIOInit(echo, GPIO_INPUT);
	pin_echo = echo;
	GPIOInit(trigger, GPIO_OUTPUT);
	pin_trigger = trigger;
	return true;
}

int16_t HcSr04ReadDistanceCentimeters(void){
	uint16_t cont=0;
	uint16_t distance_cm;
	GPIOOn(pin_trigger);
	DelayUs(10);
	GPIOOff(pin_trigger);
	while (GPIORead(pin_echo) == 0);
	while (GPIORead(pin_echo) == 1){
		DelayUs(1);
	    cont++;
	}
	distance_cm = cont/58;
	return distance_cm;
}

int16_t HcSr04ReadDistanceInches(void){
	int16_t cont=0;
	int16_t distance_in;
	GPIOOn(pin_trigger);
	DelayUs(10);
	GPIOOff(pin_trigger);
	while (GPIORead(pin_echo) == 0);
	while (GPIORead(pin_echo) == 1){
		DelayUs(1);
	    cont++;
	}
	distance_in = cont/148;
	return distance_in;
}

bool HcSr04Deinit(gpio_t echo, gpio_t trigger){
	GPIODeinit();
	return true;
}


/*==================[end of file]============================================*/

