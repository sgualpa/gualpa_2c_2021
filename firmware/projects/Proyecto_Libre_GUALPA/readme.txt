﻿Proyecto Libre: Bastón inteligente para personas no videntes o con visión disminuida.

Descripción general:
Esta aplicación activa una alarma, tanto visual como sonora, cuando el extremo inferior de un bastón para personas no videntes o con visión disminuida está cerca de un objeto.
La medición de distancia se realiza con un sensor de ultrasonido que se activa cuando, a través de un acelerómetro, se registran cambios en la aceleración del bastón.

Conexión del hardware:
|  Sensor de ultrasonido (HcSr04) |   EDU-CIAA	    |
|:-------------------------------:|:----------------|
|          ECHO                   | 	GPIO_T_COL0 |
|          TRIGGER                | 	GPIO_T_FIL2 |
|          +5V                    |  	+5V  	    |
|          GND                    |  	GND 	    |

|   Acelerómetro (MMA7260Q)  |   EDU-CIAA	|
|:--------------------------:|:-----------------|
|           PINX	     | 	   CH1		|
|           PINY	     | 	   CH2 		|
|           PINZ	     | 	   CH3		|
|           GS1              |  GPIO_T_FIL3 	|
|           GS2              |  GPIO_T_FIL0     |
|           VCC              | 	  +3.3V		|
|           GND              | 	   GND		|
|           SM               | 	  +3.3V		|

|     Buzzer     |   EDU-CIAA	 |
|:--------------:|:--------------|
|      OUT	 |    T_FIL1     |
|      GND	 |     GND 	 |
