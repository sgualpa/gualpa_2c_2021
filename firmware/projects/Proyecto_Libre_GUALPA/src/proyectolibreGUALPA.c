/*! @mainpage Proyecto Libre: Bastón inteligente para personas no videntes o con visión disminuida
 *
 * \section genDesc General Description
 *
 * Esta aplicación activa una alarma, tanto visual como sonora, cuando el extremo inferior de un bastón para personas no videntes o con visión disminuida está cerca de un objeto.
 * La medición de distancia se realiza con un sensor de ultrasonido que se activa cuando, a través de un acelerómetro, se registran cambios en la aceleración del bastón.
 *
 * \section hardConn Hardware Connection
 *
 * |  Sensor de ultrasonido (HcSr04) |   EDU-CIAA	|
 * |:-------------------------------:|:-------------|
 * |              ECHO               | 	GPIO_T_COL0 |
 * |        	TRIGGER              | 	GPIO_T_FIL2 |
 * | 	          +5V                |  	+5V  	|
 * | 	          GND                |  	GND 	|
 *
 * |   Acelerómetro (MMA7260Q)  |   EDU-CIAA	|
 * |:--------------------------:|:--------------|
 * |       	   PINX	        	| 	   CH1		|
 * |       	   PINY	        	| 	   CH2 		|
 * |       	   PINZ	        	| 	   CH3		|
 * |       	   GS1          	|  GPIO_T_FIL3 	|
 * |       	   GS2          	|  GPIO_T_FIL0  |
 * |       	   VCC          	| 	  +3.3V		|
 * |       	   GND          	| 	   GND		|
 * |       	   SM          	    | 	  +3.3V		|
 *
 * |     Buzzer     |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   	  OUT	 	|    T_FIL1     |
 * |   	  GND	 	| 	   GND 		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 11/10/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Solange Gualpa
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/proyectolibreGUALPA.h"       /* <= own header */
#include "MMA7260Q.h"
#include "HcSr04.h"
#include "timer.h"
#include "uart.h"
#include "systemclock.h"
#include "led.h"
#include "buzzer.h"

/*==================[macros and definitions]=================================*/
/** @def BAUD_RATE
 * @brief Número de unidades de señal por segundo.
 */
#define BAUD_RATE 115200

/** @def THRESHOLD
 * @brief Umbral que debe superar el cambio de aceleración para comenzar la medición del sensor de ultrasonido.
 */
#define THRESHOLD 50

/** @def THRESHOLD_CM
 * @brief Distancia en cm a la cual el usuario está en peligro, por lo que se activa la alarma.
 */
#define THRESHOLD_CM 50

/** @def DELTA_TIME
 * @brief Tiempo de muestreo en ms.
 */
#define DELTA_TIME 50

/** @def GRAVITY
 * @brief Aceleración de la gravedad en m/s^2.
 */
#define GRAVITY 9.8

/** @fn abs(a)
 * @brief Función valor absoluto.
 */
#define abs(a) ((a)<0?-(a):(a))

/*==================[internal data definition]===============================*/
bool reading_flag = false;     /**< Bandera para activar la medición del ultrasonido*/
float acc_now_x = 0;           /**< Medición actual en X del acelerómetro*/
float acc_next_x = 0;          /**< Medición siguiente en X del acelerómetro*/
float acc_now_y = 0;           /**< Medición actual en Y del acelerómetro*/
float acc_next_y = 0;          /**< Medición siguiente en Y del acelerómetro*/
float acc_now_z = 0;           /**< Medición actual en Z del acelerómetro*/
float acc_next_z = 0;          /**< Medición siguiente en Z del acelerómetro*/
float delta_acc_x = 0;         /**< Diferencia de aceleración en eje X*/
float delta_acc_y = 0;         /**< Diferencia de aceleración en eje Y*/
float delta_acc_z = 0;         /**< Diferencia de aceleración en eje Z*/
int16_t distance_cm;           /**< Medición del sensor de ultrasonido*/

/*==================[internal functions declaration]=========================*/
/**@fn float Amplifier(float acc_now, float acc_then)
 * @brief  Función que amplifica el valor de aceleración y calcula el cambio en la misma.
 * @return float Cambio en aceleración
 */
float Amplifier(float acc_now, float acc_then){
	/*La ecuación ideal que gobierna el comportamiento del amplificador diferencial es:
	 * Vo = A (V+ - V-)
	 * siendo A la ganancia*/
	float delta_acc = 0;
	if ((abs(acc_now)>0) && (abs(acc_now)<1))
		acc_now = acc_now*1000;
	else acc_now = acc_now*100;
	if ((abs(acc_then)>0) && (abs(acc_then)<1))
		acc_then = acc_then*1000;
	else acc_then = acc_then*100;

	delta_acc = abs(acc_now - acc_then);
	return delta_acc;
}

/**@fn void Measure(void)
 * @brief  Función que mide si el bastón se está moviendo, a través del acelerómetro, para iniciar la medición del sensor de ultrasonido.
 * @return None
 */
void Measure(void){
	/**< Medición de aceleración en los diferentes ejes*/
	acc_now_x = ReadXValue();
	acc_now_y = ReadYValue();
	acc_now_z = ReadZValue();

	/**< Cambio en aceleración en los diferentes ejes*/
	delta_acc_x = Amplifier(acc_now_x, acc_next_x);
	delta_acc_y = Amplifier(acc_now_y, acc_next_y);
	delta_acc_z = Amplifier(acc_now_z, acc_next_z);

	/**< Bandera para comenzar medición con sensor de ultrasonido*/
	if ((delta_acc_x > THRESHOLD) || (delta_acc_y > THRESHOLD) || (delta_acc_z > THRESHOLD)){
		reading_flag = true;
	}
	else reading_flag = false;

	/**< Actualización de valores de aceleración en los diferentes ejes*/
	acc_next_x = acc_now_x;
	acc_next_y = acc_now_y;
	acc_next_z = acc_now_z;
}

/**@fn void Alert(void)
 * @brief  Función que inicia la medición del sensor de ultrasonido y activa la alerta (leds y buzzer).
 * @return None
 */
void Alert(void){

	if (reading_flag){
		distance_cm = HcSr04ReadDistanceCentimeters();   /**< Medición en cm*/
		if (distance_cm < THRESHOLD_CM){                 /**< Comparación para activar la alarma visual y sonora*/
			LedOn(LED_RGB_B);
			LedOn(LED_1);
			LedOn(LED_2);
			LedOn(LED_3);
			BuzzerInit();
			BuzzerOn();
		}
		else {
		BuzzerOff();                                    /**< Apagado de la alarma en caso que la distancia al obstáculo sea mayor a 50 cm*/
		LedsOffAll();
		}
	}
	else {
		BuzzerOff();                                   /**< Apagado de la alarma en caso que el bastón no esté en movimiento*/
		LedsOffAll();
	}
}

/**@fn void Message(void)
 * @brief  Función que muestra la medición en cm en la computadora.
 * @return None
 */
void Message(void){
uint8_t key;
UartReadByte(SERIAL_PORT_PC, &key);
if ((key='m')) {
	UartSendString(SERIAL_PORT_PC, "La distancia al objeto es: ");
	UartSendString(SERIAL_PORT_PC, UartItoa(distance_cm, 10));
	UartSendString(SERIAL_PORT_PC, " cm\r\n");
}
}


/*==================[external data definition]===============================*/
timer_config my_timer_AD = {TIMER_A,DELTA_TIME,&Measure};         /**< Medición de acelerómetro*/
timer_config my_timer_measure = {TIMER_B,200,&Alert};             /**< Medición de sensor de ultrasonido y alerta*/
serial_config my_port = {SERIAL_PORT_PC,BAUD_RATE,&Message};      /**< Control del puerto*/

/**@fn void SysInit()
 * @brief  Función que contiene las funciones de inicialización del sistema
 * @return None
 */
void SysInit(void){
	SystemClockInit();
	LedsInit();
	LedsOffAll();
	MMA7260QInit(GPIO_T_FIL3, GPIO_T_FIL0);
	HcSr04Init(GPIO_T_COL0, GPIO_T_FIL2);
	TimerInit(&my_timer_AD);
	TimerStart(TIMER_A);
	TimerInit(&my_timer_measure);
	TimerStart(TIMER_B);
	UartInit(&my_port);
}

/*==================[external functions definition]==========================*/
/**@fn int main(void)
 * @brief  Función main de aplicación del proyecto
 * @return 0
 */
int main(void){

	SysInit();

    while(1){
    }
	return 0;
}

/*==================[end of file]============================================*/

