/*! @mainpage Proyecto Libre: Bastón inteligente para personas no videntes o con visión disminuida.
 *
 * \section genDesc General Description
 *
 * Esta aplicación activa una alarma, tanto visual como sonora, cuando el extremo inferior de un bastón para personas no videntes o con visión disminuida está cerca de un objeto.
 * La medición de distancia se realiza con un sensor de ultrasonido que se activa cuando, a través de un acelerómetro, se registran cambios en la aceleración del bastón.
 *
 * <a href="https://drive.google.com/file/d/18UMK1IvXcLo3hQElMe_ZNvTGp1k7i-50/view?usp=sharing">Explicación de funcionamiento</a>
 *
 * \section hardConn Hardware Connection
 *
 * |  Sensor de ultrasonido (HcSr04) |   EDU-CIAA	|
 * |:-------------------------------:|:-------------|
 * |              ECHO               | 	GPIO_T_COL0 |
 * |        	TRIGGER              | 	GPIO_T_FIL2 |
 * | 	          +5V                |  	+5V  	|
 * | 	          GND                |  	GND 	|
 *
 * |   Acelerómetro (MMA7260Q)  |   EDU-CIAA	|
 * |:--------------------------:|:--------------|
 * |       	   PINX	        	| 	   CH1		|
 * |       	   PINY	        	| 	   CH2 		|
 * |       	   PINZ	        	| 	   CH3		|
 * |       	   GS1          	|  GPIO_T_FIL3 	|
 * |       	   GS2          	|  GPIO_T_FIL0  |
 * |       	   VCC          	| 	  +3.3V		|
 * |       	   GND          	| 	   GND		|
 * |       	   SM          	    | 	  +3.3V		|
 *
 * |     Buzzer     |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   	  OUT	 	|    T_FIL1     |
 * |   	  GND	 	| 	   GND 		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 11/10/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Solange Gualpa
 *
 */

#ifndef proyectolibre_H
#define proyectolibre_H

/*==================[inclusions]=============================================*/


int main(void);

/*==================[cplusplus]==============================================*/

/*==================[external functions declaration]=========================*/
/**@fn void Measure(void)
 * @brief  Función que mide si el bastón se está moviendo, a través del acelerómetro, para iniciar la medición del sensor de ultrasonido.
 * @return None
 */
void Measure(void);

/**@fn void Alert(void)
 * @brief  Función que iniciar la medición del sensor de ultrasonido y activa la alerta (leds y buzzer).
 * @return None
 */
void Alert(void);

/**@fn void Message(void)
 * @brief  Función que muestra la medición del sensor de ultrasonido en la computadora.
 * @return None
 */
void Message(void);
/*==================[end of file]============================================*/

#endif /* #ifndef proyectolibre_H */

