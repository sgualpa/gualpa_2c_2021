/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/display.h"       /* <= own header */

#include "DisplayITS_E0803.h"
#include "gpio.h"
#include "delay.h"
#include "chip.h"
#include "systemclock.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	SystemClockInit();
	gpio_t pin_es[7]= {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};
	uint16_t valor_ingresado=123;
	ITSE0803Init(pin_es);
	ITSE0803DisplayValue(valor_ingresado);
	ITSE0803ReadValue();

	return 0;
}

/*==================[end of file]============================================*/

