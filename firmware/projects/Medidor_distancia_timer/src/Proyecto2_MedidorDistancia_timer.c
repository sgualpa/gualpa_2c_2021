/*! @mainpage Project 2: Distance measurer with timer
 *
 * \section genDesc General Description
 *
 * This application makes measurements in centimeters and inches
 *
 * \section hardConn Hardware Connection
 *
 * | Device1	    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |    GPIO_T_FIL2 | 	ECHO		|
 * | 	GPIO_T_FIL3	| 	TRIGGER		|
 * | 	+5V	 	    | 	+5V  		|
 * | 	GND 	 	| 	GND 		|
 *
 * | Device2	    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |    GPIO_LCD_1  | 	D1  		|
 * | 	GPIO_LCD_2	| 	D2  		|
 * | 	GPIO_LCD_3  | 	D3  		|
 * | 	GPIO_LCD_4 	| 	D4  		|
 * |    GPIO_1      | 	SEL_0  		|
 * | 	GPIO_3  	| 	SEL_1  		|
 * | 	GPIO_5      | 	SEL_2  		|
 * | 	+5V	 	    | 	+5V  		|
 * | 	GND 	 	| 	GND 		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 16/09/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Solange Gualpa
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto2_MedidorDistancia_timer.h"       /* <= own header */

#include "gpio.h"
#include "systemclock.h"
#include "medidor_distancia.h"
#include "led.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "bool.h"
#include "timer.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/
void Measurer (void);
/*==================[external data definition]===============================*/
bool encendido_flag = false;
bool hold_flag = true;
timer_config my_timer = {TIMER_A,1000,&Measurer};

/*==================[external functions definition]==========================*/
void Tec_1(void){
	encendido_flag =! encendido_flag;
}

void Tec_2(void){
	hold_flag =! hold_flag;
}

void Measurer (void){
HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
gpio_t pin_es[7]= {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};
int16_t distance_cm, distance_in;

SwitchActivInt(SWITCH_1, &Tec_1);
SwitchActivInt(SWITCH_2, &Tec_2);

if (encendido_flag){
	distance_cm = HcSr04ReadDistanceCentimeters();   //Valor medido en cm
	distance_in = HcSr04ReadDistanceInches();        //Valor medido en pulgadas

	if (hold_flag){
		// Muestro distancia medida utilizando los leds:
		if (distance_cm>0 && distance_cm<10){
			LedOn(LED_RGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
			}
		else if (distance_cm>=10 && distance_cm<20){
				LedOn(LED_RGB_B);
				LedOn(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
				}
			else if (distance_cm>=20 && distance_cm<30){
					LedOn(LED_RGB_B);
					LedOn(LED_1);
					LedOn(LED_2);
					LedOff(LED_3);
					}
				else {LedOn(LED_RGB_B);
					  LedOn(LED_1);
					  LedOn(LED_2);
					  LedOn(LED_3);
					  }
		// Muestro distancia medida en cm en el display
		ITSE0803Init(pin_es);
		ITSE0803DisplayValue(distance_cm);
		}}

else {
	// Tecla 1 no encendida
	LedsOffAll();
	ITSE0803Init(pin_es);
	ITSE0803DisplayValue(0);
	}
}

void SisInit(void){
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	TimerInit(&my_timer); //Inicializa el timer. Recibe la variable my_timer, que tiene un puntero a función que llama repetitivamente
	TimerStart(TIMER_A);  //Enciende el timer
}

//Comienza el main principal
int main(void){
	SisInit();

	while(1){
	}

	return 0;
}

/*==================[end of file]============================================*/

