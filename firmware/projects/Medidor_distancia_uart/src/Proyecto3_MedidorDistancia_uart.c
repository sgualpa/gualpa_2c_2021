/*! @mainpage Project 3-Distance measurer with UART
 *
 * \section genDesc General Description
 *
 * This application makes measurements in centimeters and inches, using the universal asynchronous receiver/transmitter (UART).
 * The measurement starts and ends by pressing key 1 or the letter 'o' in the keyboard of the computer, and it's is maintained by pressing key 2 or the letter 'h'.
 *
 * \section hardConn Hardware Connection
 *
 * | Device1	    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |    GPIO_T_FIL2 | 	ECHO		|
 * | 	GPIO_T_FIL3	| 	TRIGGER		|
 * | 	+5V	 	    | 	+5V  		|
 * | 	GND 	 	| 	GND 		|
 *
 * | Device2	    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |    GPIO_LCD_1  | 	D1  		|
 * | 	GPIO_LCD_2	| 	D2  		|
 * | 	GPIO_LCD_3  | 	D3  		|
 * | 	GPIO_LCD_4 	| 	D4  		|
 * |    GPIO_1      | 	SEL_0  		|
 * | 	GPIO_3  	| 	SEL_1  		|
 * | 	GPIO_5      | 	SEL_2  		|
 * | 	+5V	 	    | 	+5V  		|
 * | 	GND 	 	| 	GND 		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 16/09/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Solange Gualpa
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto3_MedidorDistancia_uart.h"       /* <= own header */

#include "gpio.h"
#include "systemclock.h"
#include "medidor_distancia.h"
#include "led.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/
/** @fn void Measurer(void)
 * @brief Initialize the pines on the EDU-CIAA, makes measurements and show them using leds and in the display.
 * @param[in] No parameter
 */
void Measurer (void);

/** @fn void MeasurerUart(void)
 * @brief Initialize the pines on the EDU-CIAA, makes measurements and show them using leds, in the display and in the computer.
 * @param[in] No parameter
 */
void MeasurerUart (void);
/*==================[external data definition]===============================*/
bool encendido_flag = false;
bool hold_flag = true;
timer_config my_timer = {TIMER_A,1000,&Measurer};
serial_config UART_USB = {SERIAL_PORT_PC,115200,&MeasurerUart};
/*==================[external functions definition]==========================*/
void Tec_1(void){
	encendido_flag =! encendido_flag;
}

void Tec_2(void){
	hold_flag =! hold_flag;
}

void Measurer (void){
HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);                                                       //Inicializo los pines del ultrasonido
gpio_t pin_es[7]= {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5}; //Inicializo pines del display
int16_t distance_cm, distance_in;

SwitchActivInt(SWITCH_1, &Tec_1);
SwitchActivInt(SWITCH_2, &Tec_2);

if (encendido_flag){
	distance_cm = HcSr04ReadDistanceCentimeters();   //Valor medido en cm
	distance_in = HcSr04ReadDistanceInches();        //Valor medido en pulgadas

	if (hold_flag){
		// Muestro distancia medida utilizando los leds:
		if (distance_cm>0 && distance_cm<10){
			LedOn(LED_RGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
			}
		else if (distance_cm>=10 && distance_cm<20){
				LedOn(LED_RGB_B);
				LedOn(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
				}
			else if (distance_cm>=20 && distance_cm<30){
					LedOn(LED_RGB_B);
					LedOn(LED_1);
					LedOn(LED_2);
					LedOff(LED_3);
					}
				else {LedOn(LED_RGB_B);
					  LedOn(LED_1);
					  LedOn(LED_2);
					  LedOn(LED_3);
					  }
		// Muestro distancia medida en cm en el display
		ITSE0803Init(pin_es);
		ITSE0803DisplayValue(distance_cm);
		}}

else {
	// Tecla 1 no encendida
	LedsOffAll();
	ITSE0803Init(pin_es);
	ITSE0803DisplayValue(0);
	}
}

void MeasurerUart (void){
HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);                                                       //Inicializo los pines del ultrasonido
gpio_t pin_es[7]= {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5}; //Inicializo pines del display
int16_t distance_cm, distance_in;

uint8_t tec;
UartReadByte(SERIAL_PORT_PC, &tec);

switch (tec){
	case 'o':
		Tec_1();
		break;
	case 'h':
		Tec_2();
		break;
}

if (encendido_flag){
	distance_cm = HcSr04ReadDistanceCentimeters();   //Valor medido en cm
	distance_in = HcSr04ReadDistanceInches();        //Valor medido en pulgadas

	if (hold_flag){
		// Muestro distancia medida utilizando los leds:
		if (distance_cm>2 && distance_cm<10){
			LedOn(LED_RGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
			}
		else if (distance_cm>=10 && distance_cm<20){
				LedOn(LED_RGB_B);
				LedOn(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
				}
			else if (distance_cm>=20 && distance_cm<130){
					LedOn(LED_RGB_B);
					LedOn(LED_1);
					LedOn(LED_2);
					LedOff(LED_3);
					}
				else {LedOn(LED_RGB_B);
					  LedOn(LED_1);
					  LedOn(LED_2);
					  LedOn(LED_3);
					  }
		// Muestro distancia medida en cm en el display
		ITSE0803Init(pin_es);
		ITSE0803DisplayValue(distance_cm);
		}}

else {
	// Tecla 1 no encendida
	LedsOffAll();
	ITSE0803Init(pin_es);
	ITSE0803DisplayValue(0);
	}

UartSendString(SERIAL_PORT_PC, UartItoa(distance_cm, 10)); // Base = 10
UartSendString(SERIAL_PORT_PC, " ");
UartSendString(SERIAL_PORT_PC, "cm");
UartSendString(SERIAL_PORT_PC, "\r\n");
UartSendString(SERIAL_PORT_PC, UartItoa(distance_in, 10));
UartSendString(SERIAL_PORT_PC, " ");
UartSendString(SERIAL_PORT_PC, "in");
UartSendString(SERIAL_PORT_PC, "\r\n");
}

void SisInit(void){
	SystemClockInit();
	LedsInit();
	TimerInit(&my_timer); //Inicializa el timer. Recibe la variable my_timer, que tiene un puntero a función que llama repetitivamente
	TimerStart(TIMER_A)  ;  //Enciende el timer
	UartInit(&UART_USB);  //Inicializa la UART
}

//Comienza el main principal
int main(void){
	SisInit();

	while(1){
	}

	return 0;
}

/*==================[end of file]============================================*/

