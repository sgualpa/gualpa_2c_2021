/*! @mainpage Parcial 2C2021 - GUALPA
 *
 * \section genDesc General Description
 *
 * Se pretende diseñar un dispositivo basado en la EDU-CIAA que se utilizará como control de un sistema de llenado de cajas en una línea de producción.
 *
 * \section hardConn Hardware Connection
 *
 * |   Tcrt5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	+5V 		|
 * | 	PIN2	 	| 	GPIO_T_COL0 |
 * | 	PIN3	 	| 	GND			|
 *
 * |   Balanza		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Vref	 	| 	+5V 		|
 * | 	Salida	 	| 	CH1         |
 * | 	GND 	 	| 	GND			|
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 11/11/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Solange Gualpa
 *
 */

#ifndef _PARCIAL2C2021-GUALPA_H
#define _PARCIAL2C2021-GUALPA_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);
#include "Tcrt5000.h"
#include "balanza.h"
#include "timer.h"
#include "uart.h"
#include "led.h"
#include "bool.h"
#include "gpio.h"
#include "switch.h"
/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/
/**@fn void SysInit()
 * @brief  Función que contiene las funciones de inicialización del sistema
 * @return None
 */
void SysInit(void);

/**@fn bool MedicionBalanza(void)
 * @brief  Función que obtiene la medicion de la balanza
 * @return true if no error
 */
bool MedicionBalanza(void);

/**@fn bool Mensaje(void)
 * @brief  Informa por el puerto serie los tiempos de llenado máximo y mínimo de dicho lote
 * @return true if no error
 */
bool Mensaje(void);
/*==================[end of file]============================================*/


#endif /* #ifndef _PARCIAL2C2021-GUALPA_H */

