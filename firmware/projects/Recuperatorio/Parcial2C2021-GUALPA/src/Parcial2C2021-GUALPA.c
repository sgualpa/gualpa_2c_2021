/*! @mainpage Parcial 2C2021 - GUALPA
 *
 * \section genDesc General Description
 *
 * Se pretende diseñar un dispositivo basado en la EDU-CIAA que se utilizará como control de un sistema de llenado de cajas en una línea de producción.
 *
 * \section hardConn Hardware Connection
 *
 * |   Tcrt5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	+5V 		|
 * | 	PIN2	 	| 	GPIO_T_COL0 |
 * | 	PIN3	 	| 	GND			|
 *
 * |   Balanza		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Vref	 	| 	+5V 		|
 * | 	Salida	 	| 	CH1         |
 * | 	GND 	 	| 	GND			|
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 11/11/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Solange Gualpa
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Parcial2C2021-GUALPA.h"       /* <= own header */
#include "Tcrt5000.h"
#include "balanza.h"
#include "timer.h"
#include "uart.h"
#include "led.h"
#include "bool.h"
#include "gpio.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/
#define BAUD_RATE 115200
#define PESO 20           // Valor en kg que tiene que alcanzar la caja
#define TIEMPO 1000       // 1000 ms
#define LOTE 15           // Cantidad de cajas por lote

typedef struct {
	uint16_t caja;
	uint16_t tiempo_llenado;
} cajas;
/*==================[internal data definition]===============================*/
bool medicion_tcrt5000 = false;      //Bandera para saber si las cajas vacías están en la posición de llenado
uint16_t peso_caja = 0;              //Valor del peso de la caja en kg
uint16_t cont_caja = 0;              //Contador para el numero de cajas
uint16_t cont_tiempo = 0;            //Contador para el tiempo de llenado
cajas cajas_llenas[LOTE];            //Tiempo llenado caja
bool encendido_flag = false;         //Bandera para encendido por tecla
bool apagado_flag = false;           //Bandera para apagado por tecla
/*==================[internal functions declaration]=========================*/
bool MedicionBalanza(void);

void Tec_1(void){
	encendido_flag =! encendido_flag;
}

void Tec_2(void){
	apagado_flag =! apagado_flag;
}

/*==================[external data definition]===============================*/
timer_config my_timer_medicion = {TIMER_A,TIEMPO,&MedicionBalanza};
serial_config my_port = {SERIAL_PORT_PC,BAUD_RATE,&Mensaje};
/*==================[external functions definition]==========================*/

/**@fn void SysInit()
 * @brief  Función que contiene las funciones de inicialización del sistema
 * @return None
 */
void SysInit(void){
	SystemClockInit();
	SwitchesInit();
	LedsInit();
	LedsOffAll(); //Arranca con todos los leds apagados
	Tcrt5000Init(GPIO_T_COL0);
	BalanzaInit(); //Inicialización de la balanza
	UartInit(&my_port);
	TimerInit(&my_timer_medicion);
	}

/**@fn bool MedicionBalanza(void)
 * @brief  Función que obtiene la medicion de la balanza
 * @return true if no error
 */
bool MedicionBalanza(void){
	medicion_tcrt5000 = Tcrt5000State();
	if(medicion_tcrt5000 == false)
		LedOn(LED_1);
	else {
		LedOff(LED_1);
		LedOn(LED_2);
		BalanzaMedicion(peso_caja);
	}
	if (peso_caja == PESO){
		LedOff(LED_2);
		LedOn(LED_1);
		cont_caja = cont_caja+1;
		cajas_llenas[cont_caja].caja = cont_caja;
		cont_tiempo = cont_tiempo+1;
		cajas_llenas[cont_caja].tiempo_llenado = cont_tiempo*1000*TIEMPO;  //Tiempo en segundos que demora en llenarse la caja
		UartSendString(SERIAL_PORT_PC, "Tiempo de llenado de caja ");
		UartSendString(SERIAL_PORT_PC, UartItoa(cont_caja, 10));
		UartSendString(SERIAL_PORT_PC, " ");
		UartSendString(SERIAL_PORT_PC, UartItoa(cajas_llenas[cont_caja].tiempo_llenado, 10));
		UartSendString(SERIAL_PORT_PC, " seg.\r\n");
		peso_caja = 0;
		cont_tiempo = 0;
	}
	else cont_tiempo = cont_tiempo+1;
	medicion_tcrt5000 = false;
	return true;
}

/**@fn bool Mensaje(void)
 * @brief  Informa por el puerto serie los tiempos de llenado máximo y mínimo de dicho lote
 * @return true if no error
 */
bool Mensaje(void){
	if (cont_caja == LOTE){ //Si se llegó a llenar el lote (15 cajas)
		cont_caja = 0;
		uint16_t tiempo_max = cajas_llenas[0].tiempo_llenado;
		uint16_t tiempo_min = cajas_llenas[0].tiempo_llenado;
		for (uint8_t i=1; i<LOTE; i++){
			if(cajas_llenas[i].tiempo_llenado > tiempo_max)
				tiempo_max = cajas_llenas[i].tiempo_llenado;
			if(cajas_llenas[i].tiempo_llenado < tiempo_min)
				tiempo_min = cajas_llenas[i].tiempo_llenado;
		}
		UartSendString(SERIAL_PORT_PC, "Tiempo de llenado máximo: ");
		UartSendString(SERIAL_PORT_PC, UartItoa(tiempo_max, 10));
		UartSendString(SERIAL_PORT_PC, " seg. \r\n");
		UartSendString(SERIAL_PORT_PC, "Tiempo de llenado mínimo: ");
		UartSendString(SERIAL_PORT_PC, UartItoa(tiempo_min, 10));
		UartSendString(SERIAL_PORT_PC, " seg. \r\n");
	}
	return true;
}

int main(void){

	SysInit();
    while(1){
    	SwitchActivInt(SWITCH_1, &Tec_1);
    	SwitchActivInt(SWITCH_2, &Tec_2);

    	if(encendido_flag)
    		TimerStart(TIMER_A);
    	else if (apagado_flag)
    		TimerStop(TIMER_A);
	}
    
	return 0;
}

/*==================[end of file]============================================*/

