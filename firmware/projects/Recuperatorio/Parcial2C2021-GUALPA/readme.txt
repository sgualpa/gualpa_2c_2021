﻿Descripción del Proyecto.
Se pretende diseñar un dispositivo basado en la EDU-CIAA que se utilizará como control de un sistema de llenado de cajas en una línea de producción.
Para esto se utilizan los periféricos:
 |   Tcrt5000	  |   EDU-CIAA	    |
 |:--------------:|:----------------|
 | 	PIN1	  | 	+5V 	    |
 | 	PIN2	  | 	GPIO_T_COL0 |
 | 	PIN3	  | 	GND         |

 |   Balanza	    |   EDU-CIAA    |
 |:----------------:|:--------------|
 | 	Vref	    | 	+5V 	    |
 | 	Salida	    | 	CH1         |
 | 	GND 	    | 	GND	    |