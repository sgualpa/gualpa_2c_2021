/*! @mainpage Project 2: Distance measurer
 *
 * \section genDesc General Description
 *
 * This application makes measurements in centimeters and inches
 *
 * \section hardConn Hardware Connection
 *
 * | Device1	    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |    GPIO_T_FIL2 | 	ECHO		|
 * | 	GPIO_T_FIL3	| 	TRIGGER		|
 * | 	+5V	 	    | 	+5V  		|
 * | 	GND 	 	| 	GND 		|
 *
 * | Device2	    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |    GPIO_LCD_1  | 	D1  		|
 * | 	GPIO_LCD_2	| 	D2  		|
 * | 	GPIO_LCD_3  | 	D3  		|
 * | 	GPIO_LCD_4 	| 	D4  		|
 * |    GPIO_1      | 	SEL_0  		|
 * | 	GPIO_3  	| 	SEL_1  		|
 * | 	GPIO_5      | 	SEL_2  		|
 * | 	+5V	 	    | 	+5V  		|
 * | 	GND 	 	| 	GND 		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 16/09/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Solange Gualpa
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto2_MedidorDistancia.h"       /* <= own header */

#include "gpio.h"
#include "systemclock.h"
#include "medidor_distancia.h"
#include "led.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "bool.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
  	SystemClockInit();
  	LedsInit();
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	SwitchesInit();                          //Inicializa las teclas

	gpio_t pin_es[7]= {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};

	int16_t distance_cm, distance_in;
	uint8_t teclas;
	bool encendido_flag = false;
	bool hold_flag = true;

	while(1){
		teclas = SwitchesRead();
		switch(teclas){
			case (SWITCH_1):
				encendido_flag =! encendido_flag;
				break;

			case (SWITCH_2):
				hold_flag =! hold_flag;
				break;

		}

	if (encendido_flag){
		distance_cm = HcSr04ReadDistanceCentimeters();   //Valor medido en cm
		distance_in = HcSr04ReadDistanceInches();        //Valor medido en pulgadas

		if (hold_flag){
			// Muestro distancia medida utilizando los leds:
				if (distance_cm>0 && distance_cm<10){
					LedOn(LED_RGB_B);
					LedOff(LED_1);
					LedOff(LED_2);
					LedOff(LED_3);
					}
					else if (distance_cm>=10 && distance_cm<20){
							LedOn(LED_RGB_B);
							LedOn(LED_1);
							LedOff(LED_2);
							LedOff(LED_3);
							}
						else if (distance_cm>=20 && distance_cm<30){
								LedOn(LED_RGB_B);
								LedOn(LED_1);
								LedOn(LED_2);
								LedOff(LED_3);
								}
							else {LedOn(LED_RGB_B);
								LedOn(LED_1);
								LedOn(LED_2);
								LedOn(LED_3);
								}
			// Muestro distancia medida en cm en el display
			ITSE0803Init(pin_es);
			ITSE0803DisplayValue(distance_cm);
		}}

	else {
		// Tecla 1 no encendida
		LedsOffAll();
		ITSE0803Init(pin_es);
		ITSE0803DisplayValue(0);
	}
	}

	return 0;
}

/*==================[end of file]============================================*/

