/*! @mainpage Project 2: Distance measurer
 *
 * \section genDesc General Description
 *
 * This application makes measurements in centimeters and inches
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * | Device1	    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |    GPIO_T_FIL2 | 	ECHO		|
 * | 	GPIO_T_FIL3	| 	TRIGGER		|
 * | 	+5V	 	    | 	+5V  		|
 * | 	GND 	 	| 	GND 		|
 *
 * | Device2	    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |    GPIO_LCD_1  | 	D1  		|
 * | 	GPIO_LCD_2	| 	D2  		|
 * | 	GPIO_LCD_3  | 	D3  		|
 * | 	GPIO_LCD_4 	| 	D4  		|
 * |    GPIO_1      | 	SEL_0  		|
 * | 	GPIO_3  	| 	SEL_1  		|
 * | 	GPIO_5      | 	SEL_2  		|
 * | 	+5V	 	    | 	+5V  		|
 * | 	GND 	 	| 	GND 		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 16/09/2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Solange Gualpa
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

