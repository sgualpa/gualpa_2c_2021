/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdio.h>
/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
typedef struct {
	uint8_t port;
	uint8_t pin;
	uint8_t dir; // 0:IN; 1:OUT
} gpioConf_t;

void BCDto7segm(uint8_t digit, gpioConf_t *vector){
	uint8_t i, constante;
	uint8_t MSK = 1;

	for(i=0; i<sizeof(vector); i++){
		printf("El puerto %d, ", vector[i].port);
		printf("pin %d, ", vector[i].pin);
		constante = digit&MSK;
		printf("tiene un %d. \n \n", constante);
		digit = digit>>1; // o en constante = (digit>>1)&MSK
}}

int main(void)
{
	uint8_t digito = 2;
	gpioConf_t puertos[CANTPUERTOS] = {{1,4,OUT}, {1,5,OUT}, {1,6,OUT}, {2,14,OUT}};

	BCDto7segm(digito,puertos);

	return 0;
}

/*==================[end of file]============================================*/
//uint8_t i;//uint8_t j=4;//for(i=0; i<CANTMICRO-1; i++){//micro[i].port = 1;//micro[i].pin = j;//j++;//}//micro[3].port = 2;//micro[3].pin = 14;
