/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
struct leds {
		uint8_t n_led; //indica el número de led a controlar (1,2 o 3)
		uint8_t n_ciclos; //indica la cantidad de ciclos de encendido/apagado
		uint8_t periodo; //indica el tiempo de cada ciclo
		uint8_t mode; //ON=1, OFF=2, TOGGLE:cambia de estado=3
	} my_leds;

void ControlLed(struct leds *led){
		uint8_t i=0;
		uint8_t j=0;
		switch (led->mode){
		case 1:
			switch (led->n_led){
				case 1: printf("Enciende el led 1. \n");
						break;
				case 2: printf("Enciende el led 2. \n");
						break;
				case 3: printf("Enciende el led 3. \n");
						break;
				}
		case 2:
			switch (led->n_led){
				case 1: printf("Apaga el led 1. \n");
						break;
				case 2: printf("Apaga el led 2. \n");
						break;
				case 3: printf("Apaga el led 3. \n");
						break;
				}
		case 3:
			for (i; i<led->n_ciclos; i++){
				switch (led->n_led){
					case 1: printf("Toggle led 1. \n");
							break;
					case 2: printf("Toggle led 2. \n");
							break;
					case 3: printf("Toggle led 3. \n");
							break;
						}
				for (j; j<led->periodo; j++){
					printf("%d es el retardo \n", j);
				}
			}
		default: break;
		}
	}

int main(void)
{

	struct leds *led_puntero;
	led_puntero=&my_leds;
	led_puntero->n_led = 1;
	led_puntero->n_ciclos = 3;
	led_puntero->periodo = 3;
	led_puntero->mode = 3;

	ControlLed(led_puntero);
	return 0;
}

/*==================[end of file]============================================*/

