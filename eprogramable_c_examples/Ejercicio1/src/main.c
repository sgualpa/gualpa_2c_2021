/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Ejercicio1/inc/main.h"

#include <stdint.h>
#include <stdio.h>
#include <string.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

int main(void)
{
	//Ejercicio 1
	//const int32_t NOMBRE = 1<<6; //pone 0 hasta que llega al lugar 6

	//Ejercicio 2
	//const int16_t NOMBRE = 3<<3; //tendre un 1 en el 3 y el 4
	//o
	//const int16_t N1 = (1<<3)|(1<<BIT4); // | es el or, pone 0 donde ambos son 0 y 1 en otro caso

	//Ejercicio 7
	//uint32_t variable = 124535;
	//const uint32_t MSK_1 = 1<<3;
	//const uint32_t MSK_2 = ~((1<<13)|(1<<14));
	//variable = variable|MSK_1;
	//variable = variable&MSK_2;

	//printf("La variable es %d",variable);

	//Ejercicio 9

	//const uint32_t VALOR = 211;
	//const uint32_t MSK = 1<<BIT4;
	//int32_t A=0;
	//if ((VALOR & MSK) != FALSE)
	//{ A=0xaa;
	//}
	//printf("%d",A);

	//Ejercicio 12
	//int16_t a=-1; //Declaración de variable
	//int16_t *pointer; //Declaración de variable puntero
	//printf("El numero es: %d \n", a);
	//const int16_t MSK_3 = ~(1<<BIT4);
	//pointer=&a; //Asignación de la dirección memoria de a
	//*pointer = *pointer&MSK_3;
	//printf("El numero es: %d", *pointer);

	//Ejercicio 14
	//struct alumno{
		//char nombre[12];
		//char apellido[20];
		//int edad;
	//}; //si pongo aca alumno_1 y abajo lo inicializo no funciona

	//struct alumno alumno_1= {"Solange", "Gualpa", 24}; //o alumno alumno_1 (declarando struct typedef(
	//printf("%s es mi nombre \n",alumno_1.nombre);
	//printf("%s es mi apellido \n",alumno_1.apellido);
	//printf("%d es mi edad \n",alumno_1.edad);

	//struct alumno *alumno_puntero;
	//struct alumno alumno_2;
	//alumno_puntero = &alumno_2;
	//strcpy(alumno_puntero->nombre, "Nancy");
	//strcpy(alumno_puntero->apellido, "Rohrig");
	//alumno_puntero->edad = 27;
	//printf("%s es mi nombre \n",alumno_puntero->nombre);
	//printf("%s es mi apellido \n",alumno_puntero->apellido);
	//printf("%d es mi edad",alumno_puntero->edad);

	//Ejercicio 16
	//uint32_t b= 0x01020304; //b1=01, b2=02, b3=03, b4=04
	//printf("%d \n",b);
	//uint8_t b1, b2, b3, b4;
	//b4=(uint8_t)b; //casteo/truncamiento: asigna el primer byte de b3 en b4
	//b3=(uint8_t)(b>>8); // o b&0xFF en vez de b>>8
	//b2=(uint8_t)(b>16);
	//b1=(uint8_t)(b>>24);
	//printf("%d \n", b4);
	//printf("%d \n", b3);
	//printf("%d \n", b2);
	//printf("%d \n", b1);

	//union datos{
		//struct{
			//uint8_t b1, b2, b3, b4;
		//};
		//uint32_t b = 0x01020304;
	//};

	//printf("%d \n", b4);
	//printf("%d \n", b3);
	//printf("%d \n", b2);
	//printf("%d \n", b1);

	//Ejercicio 17
	uint8_t valores[15]={1,9,24,32,}
	uint16_t acumulador;
	for (i=0; i<CANT; i++){
		acumulador=acumulador+valores[i];
	}
	uint8_t promedio = acumulador/CANT; // =acumulador>>4
	printf("%d es el promedio", promedio);

	//Ejercicio 18: hacerlo

	return 0;
}

/*==================[end of file]============================================*/

